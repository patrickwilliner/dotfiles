#!/bin/bash

mkdir -p $HOME/.config/shell
mkdir -p $HOME/.config/nvim
mkdir -p $HOME/.config/vim
mkdir -p $HOME/.config/tmux
mkdir -p $HOME/.config/dircolors

cp -R .config/shell/* $HOME/.config/shell
cp -R .config/nvim/* $HOME/.config/nvim
cp -R .config/vim/* $HOME/.config/vim
cp -R .config/tmux/* $HOME/.config/tmux
cp -R .config/dircolors/* $HOME/.config/dircolors
cp ./.zshrc $HOME/
cp ./.bashrc $HOME/
