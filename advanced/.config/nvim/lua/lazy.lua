local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end

vim.opt.rtp:prepend(lazypath)

require("lazy").setup("plugins")

-- local plugins = {
--  {
--    'neovim/nvim-lspconfig',
--    dependencies = {
--      -- Automatically install LSPs to stdpath for neovim
--      'williamboman/mason.nvim',
--      'williamboman/mason-lspconfig.nvim',
--
--      -- Useful status updates for LSP
--      -- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
--      { 'j-hui/fidget.nvim', opts = {} },
--
--      -- Additional lua configuration, makes nvim stuff amazing!
--      'folke/neodev.nvim',
--    }
--  },
--  {'VonHeikemen/lsp-zero.nvim', branch = 'v3.x'},
--  {'neovim/nvim-lspconfig'},
--  {'hrsh7th/cmp-nvim-lsp'},
--  {'hrsh7th/nvim-cmp'},
--  {'L3MON4D3/LuaSnip'},
--  {
--    "folke/which-key.nvim",
--    event = "VeryLazy",
--    init = function()
--      vim.o.timeout = true
--      vim.o.timeoutlen = 300
--    end,
--    opts = {
--      -- your configuration comes here
--      -- or leave it empty to use the default settings
--      -- refer to the configuration section below
--    }
--  },
--  { 'rmagatti/goto-preview' },

--  {
--    "ThePrimeagen/harpoon",
--    branch = "harpoon2",
--    dependencies = { "nvim-lua/plenary.nvim" }
--  },

--  { "sindrets/diffview.nvim" },
--  { "lewis6991/gitsigns.nvim" },
--  { "mattn/emmet-vim" },
--   {
--   "folke/noice.nvim",
--   event = "VeryLazy",
--   opts = {
--     -- add any options here
--   },
--   dependencies = {
--     -- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
--     "MunifTanjim/nui.nvim",
--     -- OPTIONAL:
--     --   `nvim-notify` is only needed, if you want to use the notification view.
--     --   If not available, we use `mini` as the fallback
--     "rcarriga/nvim-notify",
--     }
--   },
--  {
--    "L3MON4D3/LuaSnip",
--    -- follow latest release.
--    version = "v2.*", -- Replace <CurrentMajor> by the latest released major (first number of latest release)
--    -- install jsregexp (optional!).
--    build = "make install_jsregexp"
--  }
--}
