vim.g.mapleader = " "

vim.keymap.set("i", "<c-k>", "<up>")
vim.keymap.set("i", "<c-j>", "<down>")
vim.keymap.set("i", "<c-h>", "<left>")
vim.keymap.set("i", "<c-l>", "<right>")
