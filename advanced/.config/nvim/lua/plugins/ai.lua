return {
  {
    "David-Kunz/gen.nvim",
    config = function()
      require('gen').prompts['Write Code'] = {
        prompt = "You are a senior softare engineer. Generate source code. Output code only. No comments. No helps. $input",
        replace = true
      }
    end
  }
}
