return {
  "nvim-tree/nvim-tree.lua",
  version = "*",
  lazy = false,
  dependencies = {
    "nvim-tree/nvim-web-devicons",
  },
  config = function()
    require("nvim-tree").setup {
     view = {
       width = 60
     },
     filters = {
       dotfiles = true
     },
     sort = {
       sorter = "case_sensitive"
     }
    }

    vim.opt.termguicolors = true

    vim.keymap.set("n", "<C-n>", vim.cmd.NvimTreeFindFile)
  end,
}
