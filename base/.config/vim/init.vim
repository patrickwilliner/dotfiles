let mapleader =" "

" Install plug
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Install plugins
call plug#begin(system('echo -n "$HOME/.config/nvim/plugged"'))

" colorscheme
Plug 'morhetz/gruvbox'

" status line
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" surroundings: parentheses, brackets, quotes, XML tags, and more
Plug 'tpope/vim-surround'

" color name highlighter
Plug 'ap/vim-css-color'

" emmet
Plug 'mattn/emmet-vim'

" experiment
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.1' }

Plug 'nvim-tree/nvim-web-devicons' " optional

call plug#end()

colorscheme gruvbox

set title
set go=a
set mouse=a
set nohlsearch
set clipboard+=unnamedplus
set noshowmode
set noruler
set laststatus=0
set noshowcmd
set background=dark

" tabs
set tabstop=2
set shiftwidth=2

" Some basics
nnoremap c "_c
set nocompatible
filetype plugin on
syntax on
set encoding=utf-8
set number relativenumber

" Enable autocompletion:
set wildmode=longest,list,full

" Disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
set splitbelow splitright

" In insert or command mode, move normally by using Ctrl
inoremap <C-h> <Left>
inoremap <C-j> <Down>
inoremap <C-k> <Up>
inoremap <C-l> <Right>
cnoremap <C-h> <Left>
cnoremap <C-j> <Down>
cnoremap <C-k> <Up>
cnoremap <C-l> <Right>

map <C-p> :Telescope find_files <CR>
" map <C-p> :FZF <CR>
map <C-up> :m -2 <CR>
map <C-down> :m +1 <CR>

nnoremap <C-d> <C-d>zz
nnoremap <C-u> <C-u>zz
