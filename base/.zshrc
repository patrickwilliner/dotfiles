# enable colors
autoload -U colors && colors

# history 
HISTFILE=$HOME/.zhistory
HISTSIZE=1000
SAVEHIST=1000
zstyle :compinstall filename '$HOME/.zshrc'

autoload -Uz compinit
compinit

zstyle ':completion:*' menu select
zmodload zsh/complist

setopt HIST_IGNORE_SPACE

bindkey "^A" vi-beginning-of-line
bindkey "^E" vi-end-of-line

source "$HOME/.config/shell/alias"
source "$HOME/.config/shell/env"

# colors for ls
eval `dircolors ~/.config/dircolors/dircolors`

# load syntax highlighting; should be last
source "/usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
